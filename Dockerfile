FROM golang:latest 

ARG app_env
ENV APP_ENV $app_env

COPY . /go/src/gitlab.com/akmalulginan/docker-pingpong
WORKDIR /go/src/gitlab.com/akmalulginan/docker-pingpong

RUN go get -t -v ./...
RUN go build

CMD if [ ${APP_ENV} = production ]; \
	then \
	app; \
	else \
	go get github.com/pilu/fresh && \
	fresh; \
	fi
CMD ["docker-pingpong"]
	
EXPOSE 7000